# snippets

slonked up scripts and stuff to do stuff

everything in this repository is licensed under CC0-1.0 unless specified
otherwise, use it for whatever purposes you want

# quick navigation

| C                      | Bash                                                  | JS                      |
| ---------------------- | ----------------------------------------------------- | ----------------------- |
| [unlink.c](c/unlink.c) | [arshit](shell/bash/arshit)                           | [svged.js](js/svged.js) |
| [cecho.c](c/cecho.c)   | [sshfs_mount_file.sh](shell/bash/sshfs_mount_file.sh) |                         |
|                        | [repo_build.sh](shell/bash/repo_build.sh)             |                         |
|                        | [grabber.sh](shell/bash/grabber.sh)                   |                         |
