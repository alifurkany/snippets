#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  for (int i = 1; i < argc; i++) {
    printf("\033[%dm%s", i % 2, argv[i]);
    if (i != argc - 1)
      putchar(' ');
  }
  puts("\033[0m");
}

// Compilation: cc cecho.c -o cecho
// Usage: ./cecho [STRING]...
// License: Public Domain
