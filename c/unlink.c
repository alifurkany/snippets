#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
	if (argc < 2) {
		error(1, 0, "Not enough arguments.");
	}

	short has_errored = 0;

	for (int i = 1; i < argc; i++) {
		if (unlink(argv[i]) == -1) {
			has_errored = 1;
			error(0, errno, "%s", argv[i]);
		}
	}

	if (has_errored) {
		exit(1);
	}

	return 0;
}

// Compilation: cc unlink.c -o unlink
// Usage: ./unlink FILES...
// License: Public Domain
