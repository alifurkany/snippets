#!/usr/bin/env bash
set -e

# sshfs_mount_file.sh
# Uses sshfs to mount a single file by mounting the directory to a temporary directory and symlinking the file
# I wrote this to access my Minecraft accounts saved in PrismLauncher without keeping them on the unencrypted drive of my laptop, so that's why the defaults are like that
# Also, you can't have 2 active mounts at the same time. Would be cool if someone implemented that
# MOUNT_PATH has to be the same on both hosts
# Consider creating a ssh config file before using this
# Usage:
# ./sshfs_mount_file.sh mount [MOUNT_PATH] [HOST]
# ./sshfs_mount_file.sh umount [MOUNT_PATH]

MOUNT_PATH="${2:-/home/afy/.local/share/PrismLauncher/accounts.json}"
MOUNT_DIR="$(dirname "$MOUNT_PATH")"
FILE_NAME="$(basename "$MOUNT_PATH")"
TEMP_DIR="/tmp/sshfs_mount_$UID"
HOST="${3:-remote-pc}"

mount() {
	mkdir -p "$TEMP_DIR"
	chmod o-rwx "$TEMP_DIR"
	sshfs "$HOST":"$MOUNT_DIR" "$TEMP_DIR"
	mv "$MOUNT_PATH"{,.old}
	ln -sf "$TEMP_DIR/$FILE_NAME" "$MOUNT_PATH"
}

umount() {
	mv "$MOUNT_PATH"{.old,}
	fusermount3 -u "$TEMP_DIR"
}

if [[ "$1" = "mount" ]]; then
	mount
elif [[ "$1" = "umount" ]]; then
	umount
elif [[ "$1" = "" ]]; then
	echo "You need to specify a command" >&2
	echo "Valid commands: mount, umount" >&2
	exit 1
else
	echo "Invalid command: '$1'" >&2
	exit 1
fi
