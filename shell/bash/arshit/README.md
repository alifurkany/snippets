# Artix ISO modification helper script (ARSHIT)

A small bash script to modify already built Artix ISOs

## Dependencies

- `bsdtar`
- `sponge`
- `perl`
- `xorriso`
- `arch-chroot` or `artix-chroot` (falls back to `chroot`)
- `mksquashfs`
- `unsquashfs`
- `md5sum`
- `bash` (duh)
- `sudo`
- `md5sum`

## Usage

```
Usage: ./arshit.sh extract|unsquashfs|mount|umount|mksquashfs|build|update|cleanup
Usage: ./arshit.sh chroot [program [arguments]]

Subcommands:
        extract         - Extract the ISO to a directory
        unsquashfs      - Extract the filesystem
        mount           - Mount the filesystem to /tmp/$FS_ROOT
        chroot          - Chroot into the filesystem
        umount          - Unmount the filesystem
        mksquashfs      - Build the filesystem image
        build           - Build the ISO
        update          - Does all above while updating the system
        cleanup         - Removes work directories
```

## Configuration

Configuration is done with environment variables. Export these before using the script or set them each time you use it. 

```sh
# ISO and working directory name (default: "arshit")
export ISO_NAME=arshit
# Where to extract/build the rootfs (default: "$ISO_NAME\_root")
export FS_ROOT=arshit_root
# Volume label for the ISO (default: $(echo ${ISO_NAME^^} | cut -c-12))
export VOLID=ARSHIT
# Which chroot binary to use (default $(command -v arch-chroot || command -v artix-chroot || command -v chroot))
# WHEN OVERRIDING WITH CHROOT, USE $(command -v chroot) OR IT WILL NOT MOUNT EVERYTHING
export CHROOT_COMMAND=<system dependent>
# Set to '1' to generate a MD5 hash for the filesystem image (default: unset)
export MD5SUM=
```

## Refreshing keys

From the [wiki](https://wiki.artixlinux.org/Main/Migration#Install_the_Artix_PGP_keyring), but that wont work.

```sh
pacman-key --init
pacman-key --populate artix
$(curl https://wiki.artixlinux.org/Main/Migration | grep -oP 'pacman-key --lsign-key [0-9A-F]{40}')
pacman -Sy artix-keyring
```

**`./arshit.sh update` does this if needed.**